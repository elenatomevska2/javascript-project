const options = {
    method: 'GET',
    headers: {
      accept: 'application/json',
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0OTg0YTc5NGIyOTdmMTgxOGU5MjllNWZhZWU5OTRlOSIsInN1YiI6IjY2MjBmZTEzOTY2MWZjMDE0YmZmYzcxNSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.Fn9Ghz6sZxWSouiZEpwm08DB2YkeochwfMSf_zdf3lA'
    }
  };
  
  fetch('https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=en-US&page=1&sort_by=popularity.desc', options)
    .then(response => response.json())
    .then(response => {
        const randomIndex = Math.floor(Math.random() * response.results.length);

        const movieTitle = response.results[randomIndex].title;
        document.querySelector(".title-info h1").textContent = movieTitle;

        const movieOverview = response.results[randomIndex].overview;
        document.querySelector(".title-info-synopsis").textContent = movieOverview;

        const maturityNumber = response.results[randomIndex].adult ? "18+" : "16+";
        document.querySelector(".maturity-number").textContent = maturityNumber;

        const voteAverage = response.results[randomIndex].vote_average;
        document.querySelector(".vote-average").innerHTML = `${voteAverage} <img src="/img/star.png" alt="Star" width="10px" height="10px" >`;

        const language = response.results[randomIndex].original_language;
        document.querySelector(".language").textContent = language;

        const releaseDate = response.results[randomIndex].release_date;
        const year = (new Date(releaseDate)).getFullYear(); 
        document.querySelector(".item-year span").textContent = year;

        const movieBackdrop = response.results[randomIndex].backdrop_path;
        const backdropUrl = 'https://image.tmdb.org/t/p/original' + movieBackdrop;

        document.querySelector(".bg-img").style.backgroundImage = `url(${backdropUrl})`;

        
    })


    .catch(err => console.error(err));
    
const API_URL = 'https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=4984a794b297f1818e929e5faee994e9&page=1'
const IMG_PATH = 'https://image.tmdb.org/t/p/w1280'
const SEARCH_API = 'https://api.themoviedb.org/3/search/movie?api_key=4984a794b297f1818e929e5faee994e9&query="'

const main = document.getElementById('main')
const form = document.getElementById('form')
const search = document.getElementById('search')

// Fetch movie data from url
getMovies(API_URL)

async function getMovies(url) {
    const res = await fetch(url)
    const data = await res.json()

    showMovies(data.results)  // => results are shown here
}

function showMovies(movies) {  
    main.innerHTML = ''

    movies.forEach((movie) => {  //Za sekoj film se kreira div so potrebni info
        const { title, poster_path, vote_average, overview } = movie

        const movieEl = document.createElement('div')
        movieEl.classList.add('movie')

        movieEl.innerHTML = `
        
            <img src="${IMG_PATH + poster_path}" alt="${title}">
            <div class="movie-info">
          <h3>${title}</h3>
          <span class="${getClassByRate(vote_average)}">${vote_average}</span>
            </div>
            <div class="overview">
          <h3>Overview</h3>
          ${overview}
        </div>
        `
        main.appendChild(movieEl)
    })
}

// Za boja na ocenka

function getClassByRate(vote) {
    if(vote >= 8) {
        return 'green'
    } else if(vote >= 5) {
        return 'orange'
    } else {
        return 'red'
    }
}

// fetches movies based on the search term using the SEARCH_API

form.addEventListener('submit', (e) => {
    e.preventDefault()

    const searchTerm = search.value

    if(searchTerm && searchTerm !== '') {
        getMovies(SEARCH_API + searchTerm)

        search.value = ''
    } else {
        window.location.reload()
    }
})
document.addEventListener('DOMContentLoaded', function() {
    const seriesLink = document.getElementById('seriesLink');
    if (seriesLink) {
        seriesLink.addEventListener('click', function(event) {
            event.preventDefault();

            window.location.href = seriesLink.getAttribute('href');
        });
    }
});



